/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author DELL
 */
public class UsageHistory {
    private int uhId;
    private String login;
    private String logout;
    private int employeeId;
    private String employeeName;
    private String employeeSurname;
    
    public UsageHistory() {
        this.uhId = -1;
    }
    
    
    public UsageHistory(int uhId, String login, String logout, int employeeId) {
        this.uhId = uhId;
        this.login = login;
        this.logout = logout;
        this.employeeId = employeeId;
    }

    public UsageHistory(int uhId, String login, String logout, int employeeId, String employeeName, String employeeSurname) {
        this.uhId = uhId;
        this.login = login;
        this.logout = logout;
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeSurname = employeeSurname;
    }

    public int getUhId() {
        return uhId;
    }

    public void setUhId(int uhId) {
        this.uhId = uhId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogout() {
        return logout;
    }

    public void setLogout(String logout) {
        this.logout = logout;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeSurname() {
        return employeeSurname;
    }

    public void setEmployeeSurname(String employeeSurname) {
        this.employeeSurname = employeeSurname;
    }
    
    
}
