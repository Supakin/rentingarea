/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
/**
 *
 * @author DELL
 */
public class Renting {
    private int rentingId;
    private String rentingDate;
    private String rentingTime;
    private String startDate;
    private String endDate;
    private int price;
    private int customerId;
    private int employeeId;
    private int areaId;
    private String customerName;
    private String customerSurname;
    private String employeeName;
    private String employeeSurname;
    private String areaName;
    private int areaType;
    
    public Renting() {
        this.rentingId = -1;
    }

    public Renting(int rentingId, String rentingDate, String rentingTime , String startDate, String endDate, int price, int customerId, int employeeId, int areaId) {
        this.rentingId = rentingId;
        this.rentingDate = rentingDate;
        this.rentingTime = rentingTime;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.customerId = customerId;
        this.employeeId = employeeId;
        this.areaId = areaId;

    }
    
     public Renting(int rentingId, String rentingDate, String rentingTime, String startDate, String endDate, int price, int customerId, String customerName, String customerSurname, int employeeId, String employeeName, String employeeSurname, int areaId, String areaName, int areaType) {
        this.rentingId = rentingId;
        this.rentingDate = rentingDate;
        this.rentingTime = rentingTime;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.customerId = customerId;
        this.customerName = customerName;
        this.customerSurname = customerSurname;
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeSurname = employeeSurname;
        this.areaId = areaId;
        this.areaName = areaName;
        this.areaType = areaType;
    }

    public int getRentingId() {
        return rentingId;
    }

    public void setRentingId(int rentingId) {
        this.rentingId = rentingId;
    }

    public String getRentingDate() {
        return rentingDate;
    }

    public void setRentingDate(String rentingDate) {
        this.rentingDate = rentingDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getPrice() {
        return price;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSurname() {
        return customerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeSurname() {
        return employeeSurname;
    }

    public String getRentingTime() {
        return rentingTime;
    }

    public void setRentingTime(String rentingTime) {
        this.rentingTime = rentingTime;
    }

    public void setEmployeeSurname(String employeeSurname) {
        this.employeeSurname = employeeSurname;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public int getAreaType() {
        return areaType;
    }

    public void setAreaType(int areaType) {
        this.areaType = areaType;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }
    
    
    
}
