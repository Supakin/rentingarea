/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author DELL
 */
public class Employee {
    private int employeeId;
    private String username;
    private String password;
    private String name;
    private String surname;
    private String id_number;
    private String tel;
    private int type;
    private int status;

    public Employee() {
        this.employeeId = -1;
    }
    
    
    public Employee(int employeeId, String username, String password, String name, String surname, String id_number, String tel, int type, int status) {
        this.employeeId = employeeId;
        this.username = username;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.id_number = id_number;
        this.tel = tel;
        this.type = type;
        this.status = status;
    }

    public Employee(String username, String password) {
        this.username = username;
        this.password = password;
        this.name = username;
        
    }

    
    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
     public String getTypetoString() {
        return type==0?"เจ้าของร้าน":"พนักงาน";
    }
    
    
}
