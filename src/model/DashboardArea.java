/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author DELL
 */
public class DashboardArea {
    int countArea;
    int empty;
    int full;

    public DashboardArea(int countArea, int empty, int full) {
        this.countArea = countArea;
        this.empty = empty;
        this.full = full;
    }

    public int getCountArea() {
        return countArea;
    }

    public void setCountArea(int countArea) {
        this.countArea = countArea;
    }

    public int getEmpty() {
        return empty;
    }

    public void setEmpty(int empty) {
        this.empty = empty;
    }

    public int getFull() {
        return full;
    }

    public void setFull(int full) {
        this.full = full;
    }
    
    
    
    
}
