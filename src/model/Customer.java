/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author DELL
 */
public class Customer {
    private int customerId;
    private String name;
    private String surname;
    private String tel;

    public Customer() {
        this.customerId = -1;
    }
    
    
    public Customer(int customerId, String name, String surname, String tel) {
        this.customerId = customerId;
        this.name = name;
        this.surname = surname;
        this.tel = tel;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
    
    
}
