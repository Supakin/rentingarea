/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Renting;

/**
 *
 * @author 60160019
 */
public class RentingDao {

    public static boolean insert(Renting renting) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Renting ( rentingDate, rentingTime, startDate, endDate, price, customerId, employeeId, areaId)"
                    + "   VALUES " + "( '%s', '%s', '%s', '%s', %d, %d, %d, %d  )";
            stm.execute(String.format(sql, renting.getRentingDate(),
                    renting.getRentingTime(),
                    renting.getStartDate(),
                    renting.getEndDate(),
                    renting.getPrice(),
                    renting.getCustomerId(),
                    renting.getEmployeeId(),
                    renting.getAreaId()
            ));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(RentingDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean delete(int rentingId) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "DELETE FROM Renting WHERE rentingId = %d";

            stm.execute(String.format(sql, rentingId));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(RentingDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static ArrayList<Renting> getRentings() {
        ArrayList<Renting> list = new ArrayList<Renting>();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT rentingId, rentingDate, rentingTime, startDate, endDate, price,"
                    + " Customer.customerId AS customerId, Customer.name AS customerName, Customer.surname AS customerSurname,"
                    + " Employee.employeeId AS employeeId, Employee.name AS employeeName, Employee.surname AS employeeSurname,"
                    + " Area.areaId AS areaId, Area.name AS areaName, Area.Type AS areaType "
                    + " FROM Renting JOIN Customer ON Renting.customerId = Customer.customerId"
                    + "             JOIN Employee ON Renting.employeeId = Employee.employeeId"
                    + "             JOIN Area ON Renting.areaId = Area.areaId"
                    + " ORDER BY rentingId DESC";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Renting renting = toObject(rs);
                list.add(renting);
            }
            return list;

        } catch (SQLException ex) {
            Logger.getLogger(RentingDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Renting toObject(ResultSet rs) {
        Renting renting;
        try {
            renting = new Renting(
                    rs.getInt("rentingId"),
                    rs.getString("rentingDate"),
                    rs.getString("rentingTime"),
                    rs.getString("startDate"),
                    rs.getString("endDate"),
                    rs.getInt("price"),
                    rs.getInt("customerId"),
                    rs.getString("customerName"),
                    rs.getString("customerSurname"),
                    rs.getInt("employeeId"),
                    rs.getString("employeeName"),
                    rs.getString("employeeSurname"),
                    rs.getInt("areaId"),
                    rs.getString("areaName"),
                    rs.getInt("areaType")
            );
            return renting;
        } catch (SQLException ex) {
            Logger.getLogger(RentingDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static Renting getRenting(int id) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * FROM ((Renting NATURAL JOIN Customer) NATURAL JOIN Employee) NATURAL JOIN Area WHERE rentingId = %d";
            ResultSet rs = stm.executeQuery(String.format(sql, id));
            while(rs.next()) {
                Renting renting = toObject(rs);
                Database.close();
                return renting;
            }
            return null;
            
        } catch (SQLException ex) {
            Logger.getLogger(RentingDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

}
