/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.UsageHistory;

/**
 *
 * @author DELL
 */
public class UsageHistoryDao {
    public static boolean insert(UsageHistory usage ) {
        Connection conn = Database.connect();
        try {
            Statement stm =  conn.createStatement();
            String sql = "INSERT INTO UsageHistory ( login, logout,employeeId)" +
                         "   VALUES "+ "( '%s', '%s', %d )";
            
            stm.execute(String.format(sql,  usage.getLogin(), 
                                            usage.getLogout(), 
                                            usage.getEmployeeId()                                 
                                       ));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UsageHistoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean update(UsageHistory usage) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "UPDATE UsageHistory SET  logout = '%s' "+
                         "             WHERE customerId = %d";
            boolean res = stm.execute(String.format(sql, usage.getLogout()));
            Database.close();
            return res;
        } catch (SQLException ex) {
            Logger.getLogger(UsageHistoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
  
        return false;
    }
    
    public static ArrayList<UsageHistory> getUsageHistories() {
        ArrayList <UsageHistory> list = new ArrayList<UsageHistory>();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT uhId, login, logout, UsageHistory.employeeId, Employee.name AS employeeName, Employee.surname AS employeeSurname "
                    + " FROM UsageHistory JOIN Employee ON UsageHistory.employeeId = Employee.employeeId"
                    + " ORDER BY uhId DESC";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()) {
                UsageHistory usage = toObject(rs);
                list.add(usage);
            }
            return list;
            
        } catch (SQLException ex) {
            Logger.getLogger(UsageHistoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    
    private static UsageHistory toObject(ResultSet rs) {
        UsageHistory usage;
        try {
            usage = new UsageHistory(           
                    rs.getInt("uhId"),
                    rs.getString("login"),
                    rs.getString("logout"),
                    rs.getInt("employeeId"),
                    rs.getString("employeeName"),
                    rs.getString("employeeSurname")
            );
            return usage;
        } catch (SQLException ex) {
            Logger.getLogger(UsageHistoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

 
    
    
    
   
}
