/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 *
 * @author DELL
 */
public class Database {
    static String url = "jdbc:sqlite:./db/RentMarket.db";
    static Connection conn = null;
    
    public static Connection connect() {
        if (conn!=null) return conn;
        try {
            conn = DriverManager.getConnection(url);
            return conn;
                    
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }
    
    public static void close() {
        try {
            if(conn!=null) {
                conn.close();
                conn = null;
            }
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
