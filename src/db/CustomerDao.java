/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author Windows10
 */
public class CustomerDao {
    public static boolean insert(Customer customer){
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Customer( name, surname, tel)"+
                        " VALUES "+"( '%s', '%s' , '%s')";
            stm.execute(String.format(sql, customer.getName(), customer.getSurname(), customer.getTel()));
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
        
    }
    public static boolean update(Customer customer) {
        Connection conn = Database.connect();
        System.out.println("Test :" + customer.getTel());
        try {
            Statement stm = conn.createStatement();
            String sql = "UPDATE Customer SET name = '%s', "+
                        "                     surname = '%s', "+
                        "                     tel = '%s'"+
                        "               WHERE customerId = %d";
            boolean res = stm.execute(String.format(sql, customer.getName(),
                                                         customer.getSurname(),
                                                         customer.getTel(),
                                                         customer.getCustomerId()));
            Database.close();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }public static ArrayList<Customer> getCustomer() {
        ArrayList<Customer> list = new ArrayList<Customer>();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * FROM Customer";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Customer customer = toObject(rs);
                list.add(customer);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    private static Customer toObject(ResultSet rs){
        Customer customer;
        try {
            customer = new Customer(
                    rs.getInt("customerId"),
                    rs.getString("name"),
                    rs.getString("surname"),
                    rs.getString("tel"));
            return customer;
        } catch (Exception ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);      
        }
        return null;
    }
    
    public static Customer getCustomer(int id) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * FROM Customer WHERE customerId = %d";
            ResultSet rs = stm.executeQuery(String.format(sql, id));
            if(rs.next()){
                Customer customer = toObject(rs);
                Database.close();
                return customer;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    
    public static Customer getCustomer(String tel) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * FROM Customer WHERE tel = '%s'";
            ResultSet rs = stm.executeQuery(String.format(sql, tel));
            if(rs.next()){
                Customer customer = toObject(rs);
                Database.close();
                return customer;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    public static void save(Customer customer) {
        if(customer.getCustomerId()<0){
            insert(customer);
        }else{
            update(customer);
        }
    }
    
}
