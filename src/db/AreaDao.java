/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Area;

/**
 *
 * @author DELL
 */
public class AreaDao {
    public static ArrayList<Area> getAreas(int type, int status) {
        ArrayList <Area> listAll = new ArrayList<Area>();
        ArrayList <Area> listEmpty = new ArrayList<Area>();
        ArrayList <Area> listFull = new ArrayList<Area>();
        Connection conn = Database.connect();
        int areaStatus = 0;
        try {
            Statement stm = conn.createStatement();
            String sql;
            ResultSet rs;
            if (type == -1) {
                sql = "SELECT * FROM Area";
                rs = stm.executeQuery(sql);
            } else {
                sql = "SELECT * FROM Area WHERE type = %d";
                rs = stm.executeQuery(String.format(sql,type));
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String dateStr = sdf.format(new Date()); 
            
            while(rs.next()) {
                Statement stm2 = conn.createStatement();
                String sql2 = "SELECT rentingId FROM Renting WHERE areaId = %d AND (( '%s' BETWEEN startDate AND  endDate ) OR startDate = '%s' OR endDate = '%s' )";
                ResultSet rs2 = stm2.executeQuery(String.format(sql2, rs.getInt("areaId"), dateStr, dateStr,dateStr));
                if (rs2.next())
                    areaStatus = 1;
                
                Area area = toObject(rs, areaStatus);
                
                
                listAll.add(area);
                
                if (areaStatus == 0)
                    listEmpty.add(area);
                else if (areaStatus == 1)
                    listFull.add(area);

                areaStatus = 0;
            }
            
            if (status == -1)
                return listAll;
            else if (status == 0)
                return listEmpty;
            else if (status == 1)
                return listFull;
            
        } catch (SQLException ex) {
            Logger.getLogger(AreaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    
    public static Area getArea(int id) {
        Connection conn = Database.connect();
        int status = 0;
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * FROM Area WHERE areaId = %d ";
            ResultSet rs = stm.executeQuery(String.format(sql, id));
            
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String dateStr = sdf.format(new Date());
              
            
            if (rs.next()) {
                Statement stm2 = conn.createStatement();
                String sql2 = "SELECT rentingId FROM Renting WHERE areaId = %d AND (( '%s' BETWEEN startDate AND  endDate ) OR startDate = '%s' OR endDate = '%s' )";
                ResultSet rs2 = stm2.executeQuery(String.format(sql2, rs.getInt("areaId"), dateStr, dateStr,dateStr));
                if (rs2.next())
                    status = 1;
                
                Area area = toObject(rs,status);
                Database.close();
                return area;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(AreaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    
    private static Area toObject(ResultSet rs, int status) {
        Area area;
        try {
            area = new Area(           
                    rs.getInt("areaId"),
                    rs.getString("name"),
                    rs.getString("detail"),
                    rs.getInt("type"),
                    status
            );
            return area;
        } catch (SQLException ex) {
            Logger.getLogger(AreaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
