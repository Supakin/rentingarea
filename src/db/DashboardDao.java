/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DashboardArea;

/**
 *
 * @author User
 */
public class DashboardDao {
    public static int getDailyIncome(String dateFormat) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT SUM(price) AS sum FROM Renting WHERE rentingDate LIKE '%s' ";
            ResultSet rs = stm.executeQuery(String.format(sql, dateFormat));

            return rs.getInt("sum");
        } catch (SQLException ex) {
            Logger.getLogger(DashboardDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return 0;
    }
    public static int getMonthlyIncome(String monthFormat) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT SUM(price) AS sum FROM Renting WHERE rentingDate LIKE '%s' ";
            ResultSet rs = stm.executeQuery(String.format(sql, monthFormat));

            return rs.getInt("sum");
        } catch (SQLException ex) {
            Logger.getLogger(DashboardDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return 0;
    }

    public static int getYearlyIncome(String yearFormat) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT SUM(price) AS sum FROM Renting WHERE rentingDate LIKE '%s' ";
            ResultSet rs = stm.executeQuery(String.format(sql, yearFormat));

            return rs.getInt("sum");
        } catch (SQLException ex) {
            Logger.getLogger(DashboardDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return 0;
    }

    public static DashboardArea getDashboardArea(int type) {
        Connection conn = Database.connect();
        int empty = 0;
        int full = 0;
        int countArea = 0;
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * FROM Area WHERE type = %d ";
            ResultSet rs = stm.executeQuery(String.format(sql, type));
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String dateStr = sdf.format(new Date());
              
            while (rs.next()) {
                countArea++;
                Statement stm2 = conn.createStatement();
                String sql2 = "SELECT rentingId FROM Renting WHERE areaId = %d AND (( '%s' BETWEEN startDate AND  endDate ) OR startDate = '%s' OR endDate = '%s' )";
                ResultSet rs2 = stm2.executeQuery(String.format(sql2, rs.getInt("areaId"), dateStr, dateStr,dateStr));
                if (rs2.next())
                    full++;
                else
                    empty++;
                
            }
            return new DashboardArea(countArea, empty, full);
        } catch (SQLException ex) {
            Logger.getLogger(DashboardDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
}
