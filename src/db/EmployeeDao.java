/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author max15
 */
public class EmployeeDao {
    
    public static boolean insert(Employee employee){
        Connection conn = Database.connect();
        try {
            Statement stm =conn.createStatement();
            String sql ="INSERT INTO Employee (username, password, name, surname, id_number, tel, type, status)" +
                        "   VALUES " + "( '%s', '%s', '%s', '%s', '%s', '%s', %d, %d )";
            
            stm.execute(String.format(sql, employee.getUsername(),
                                           employee.getPassword(),
                                           employee.getName(),
                                           employee.getSurname(),
                                           employee.getId_number(),
                                           employee.getTel(),
                                           employee.getType(),
                                           employee.getStatus()
                                       ));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
   
    
    public static boolean update(Employee employee){
        Connection conn = Database.connect();       
        try {
            Statement stm = conn.createStatement();
            String sql ="UPDATE Employee SET username = '%s',"+
                        "                   password = '%s',"+
                        "                   name = '%s',"+
                        "                   surname = '%s',"+
                        "                   id_number = '%s',"+
                        "                   tel = '%s',"+
                        "                   type = %d,"+
                        "                   status = %d"+
                        "             WHERE employeeId = %d ";
 
            boolean res = stm.execute(String.format(sql, employee.getUsername(),
                                           employee.getPassword(),
                                           employee.getName(),
                                           employee.getSurname(),
                                           employee.getId_number(),
                                           employee.getTel(),
                                           employee.getType(),
                                           employee.getStatus(),
                                           employee.getEmployeeId()
                                     ));
            Database.close();
            return res;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static ArrayList<Employee> getEmployees(){
        ArrayList <Employee> list = new ArrayList<Employee>();
        Connection conn = Database.connect();
        
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * FROM Employee" ;
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Employee employee = toObject(rs);
                list.add(employee);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
            Database.close();
            return null;
    }
    
    public static Employee getEmployee (String username ,String password){
        Connection conn = Database.connect();       
        try {
            Statement stm = conn.createStatement();
            String sql ="SELECT * FROM Employee WHERE username = '%s' AND password = '%s'";
            ResultSet rs = stm.executeQuery(String.format(sql, username,password));
            if(rs.next()){
                Employee employee = toObject(rs);
                Database.close();
                return employee;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null ;
    }

    
    public static Employee getEmployee (int id){
        Connection conn = Database.connect();       
        try {
            Statement stm = conn.createStatement();
            String sql ="SELECT * FROM Employee WHERE employeeId= %d";
            ResultSet rs = stm.executeQuery(String.format(sql, id));
            if(rs.next()){
                Employee employee = toObject(rs);
                Database.close();
                return employee;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null ;
    }
    
    
     public static Employee getEmployee (String username){
        Connection conn = Database.connect();       
        try {
            Statement stm = conn.createStatement();
            String sql ="SELECT * FROM Employee WHERE username= '%s'";
            ResultSet rs = stm.executeQuery(String.format(sql, username));
            if(rs.next()){
                Employee employee = toObject(rs);
                Database.close();
                return employee;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null ;
    }
    
    private static Employee toObject(ResultSet rs) {
        Employee employee;
        try {
            
            employee = new Employee(
                rs.getInt("employeeId"),
                rs.getString("username"),
                rs.getString("password"),
                rs.getString("name"),
                rs.getString("surname"),
                rs.getString("id_number"),
                rs.getString("tel"),
                rs.getInt("type"),
                rs.getInt("status")
            );
            return employee;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
         return null;
    }
    
    public static void save(Employee employee) {
        if(employee.getEmployeeId() <0){
            insert(employee);          
        }else{
            update(employee);        
        }
    }
    
}
