/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablemodel;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.Customer;

/**
 *
 * @author Windows10
 */
public class CustomerTableMode extends AbstractTableModel{
    ArrayList<Customer> customerList = new ArrayList<Customer>();
    String [] columnNames = {"ลำดับ", "รหัสลูกค้า", "ชื่อ", "นามสกุล", "หมายเลขโทรศัพท์"};
    @Override
    public int getRowCount() {
        return customerList.size();
    }
    
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
    public int getColumnCount(){
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Customer customer = customerList.get(rowIndex);
        switch(columnIndex){
            case 0: return rowIndex+1;
            case 1: return customer.getCustomerId();
            case 2: return customer.getName();
            case 3: return customer.getSurname();
            case 4: return customer.getTel();
        }
        return "";
    }
    
    public  void setData(ArrayList<Customer> customerList){
        this.customerList = customerList;
        fireTableDataChanged();
    }
}
