/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablemodel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import model.UsageHistory;

/**
 *
 * @author DELL
 */
public class UsageHistoryTableModel extends AbstractTableModel{
    ArrayList<UsageHistory> usageList = new ArrayList<UsageHistory>();
    String [] columnNames = { "ลำดับ", "เข้าสู่ระบบ", "ออกจากระบบ", "พนักงาน" };
    @Override
    public int getRowCount() {
        return usageList.size();
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        DateFormat dateInputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat dateOutputFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        UsageHistory usage = usageList.get(rowIndex);
        switch(columnIndex) {
            case 0: return rowIndex+1;
            case 1: {
                        try {
                            return dateOutputFormat.format(dateInputFormat.parse(usage.getLogin()));
                        } catch (ParseException ex) {
                            Logger.getLogger(RentingTableModel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
            case 2: {
                        try {
                            return dateOutputFormat.format(dateInputFormat.parse(usage.getLogout()));
                        } catch (ParseException ex) {
                            Logger.getLogger(RentingTableModel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
            case 3: return usage.getEmployeeName() + "  " + usage.getEmployeeSurname();
        }
        return "";
    }
    
    public void setData(ArrayList <UsageHistory> usageList) {
        this.usageList = usageList;
        fireTableDataChanged();
    }
    
}
