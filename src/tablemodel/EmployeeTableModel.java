/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablemodel;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.Employee;

/**
 *
 * @author max15
 */
public class EmployeeTableModel extends AbstractTableModel {
    ArrayList<Employee> employeeList = new ArrayList<Employee>();
    String [] columnNames = { "ลำดับ", "รหัสพนักงาน","ชื่อผู้ใช้งาน","รหัสผ่าน","ชื่อ","นามสกุล","หมายเลขบัตรประชาชน","หมายเลขโทรศัพท์","ตำแหน่ง","สถานะ" };
    @Override
    public int getRowCount() {
        return employeeList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Employee employee = employeeList.get(rowIndex);
        switch(columnIndex) {
            case 0: return rowIndex+1;
            case 1: return employee.getEmployeeId();
            case 2: return employee.getUsername();
            case 3: return employee.getPassword();
            case 4: return employee.getName();
            case 5: return employee.getSurname();
            case 6: return employee.getId_number();
            case 7: return employee.getTel();
            case 8: return employee.getType()==0?"เจ้าของร้าน":"พนักงาน";
            case 9: return employee.getStatus()==0?"ปกติ":"ออกแล้ว";
        }
        return "";
    }
    
    public void setData(ArrayList <Employee> employeeList) {
        this.employeeList = employeeList;
        fireTableDataChanged();
    }

    
    
    
}
