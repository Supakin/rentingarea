/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablemodel;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.Area;

/**
 *
 * @author DELL
 */
public class AreaTableModel extends AbstractTableModel{
    ArrayList<Area> areaList = new ArrayList<Area>();
    String [] columnNames = { "ลำดับ", "รหัสพื้นที่", "ชื่อพื้นที่", "รายละเอียด",  "ประเภท", "สถานะ" };
    @Override
    public int getRowCount() {
        return areaList.size();
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Area area = areaList.get(rowIndex);
        switch(columnIndex) {
            case 0: return rowIndex+1;
            case 1: return area.getAreaId();
            case 2: return area.getName();
            case 3: return area.getDetail();
            case 4: return area.getType()==0?"โซน A (อาหาร)":"โซน B (ของใช้)";
            case 5: return area.getStatus()==0?"ว่าง":"กำลังเช่า";
        }
        return "";
    }
    
    public void setData(ArrayList <Area> areaList) {
        this.areaList = areaList;
        fireTableDataChanged();
    }
    
}
