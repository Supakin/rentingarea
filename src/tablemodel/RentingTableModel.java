/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablemodel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import model.Renting;

/**
 *
 * @author 60160019
 */
public class RentingTableModel extends AbstractTableModel {

    ArrayList<Renting> rentingList = new ArrayList<Renting>();
    String[] columnNames = {"ลำดับ", "รหัสการเช่า", "วันที่เช่า", "วันเริ่มการขาย", "วันสิ้นสุดการขาย", "พื้นที่", "ลูกค้า", "ผู้ออกการเช่า", "ราคา"};

    public int getRowCount() {
        return rentingList.size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public String getColumnName(int column) {
        return columnNames[column];
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Renting renting = rentingList.get(rowIndex);
        //DateFormat rentingDateInputFormat = new SimpleDateFormat("yyyy-MM-dd");
        //DateFormat rentingDateOutputFormat = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat dateInputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dateOutputFormat = new SimpleDateFormat("dd-MM-yyyy");
        switch (columnIndex) {
            case 0:
                return rowIndex + 1;
            case 1:
                return renting.getRentingId();
            case 2: {
                try {
                    return dateOutputFormat.format(dateInputFormat.parse(renting.getRentingDate())) + "  " + renting.getRentingTime();
                } catch (ParseException ex) {
                    Logger.getLogger(RentingTableModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            case 3: {
                try {
                    return dateOutputFormat.format(dateInputFormat.parse(renting.getStartDate()));
                } catch (ParseException ex) {
                    Logger.getLogger(RentingTableModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            case 4: {
                try {
                    return dateOutputFormat.format(dateInputFormat.parse(renting.getEndDate()));
                } catch (ParseException ex) {
                    Logger.getLogger(RentingTableModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            case 5:
                return renting.getAreaName();
            case 6:
                return renting.getCustomerName() + "  " + renting.getCustomerSurname();
            case 7:
                return renting.getEmployeeName() + "  " + renting.getEmployeeSurname();
            case 8:
                return renting.getPrice();
        }
        return "";
    }

    public void setData(ArrayList<Renting> rentingList) {
        this.rentingList = rentingList;
        fireTableDataChanged();
    }
}
